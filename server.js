var app = require('express')()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server);



app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});

app.get('/display', function (req, res) {
  res.sendfile(__dirname + '/display.html');
});

io.sockets.on('connection', function (socket) {
  socket.on('recordMetric', function (data) {
    console.log("received:",data);
    io.sockets.emit("display", data);

  });
});

var port = Number(process.env.PORT || 3000);
server.listen(port, function() {
  console.log("Listening on " + port);
});